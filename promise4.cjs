const getBoardFromID = require("./promise1.cjs");
const getAllListsForBoardID = require("./promise2.cjs");
const getAllCardsForListID = require("./promise3.cjs");

function getThanosBoardInfo(boardID, listName) {
  if (typeof boardID === "string" && typeof listName === "string") {
    setTimeout(() => {
      return getBoardFromID(boardID)
        .then((board) => {
          console.log("Board Info:", board);
          return getAllListsForBoardID(board.id);
        })
        .then((lists) => {
          console.log("Lists for Thanos Board:", lists);
          const mindList = lists.find((list) => list.name === listName);
          if (mindList) {
            return getAllCardsForListID(mindList.id);
          } else {
            throw new Error(listName + " list not found in Thanos board");
          }
        })
        .then((cards) => {
          console.log("Cards in " + listName + " list:", cards);
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }, 2000);
  } else {
    console.log("All arguments should be a string");
  }
}

module.exports = getThanosBoardInfo;
