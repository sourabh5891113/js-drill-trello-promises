const fs = require("fs");

function getAllListsForBoardID(boardID) {
  if (typeof boardID === "string") {
    return new Promise((resolve,reject)=>{
      setTimeout(() => {
        fs.readFile("../data/lists_1.json", "utf-8", (err, data) => {
          if (err) {
            reject(err);
          } else {
            data = JSON.parse(data);
            if (data.hasOwnProperty(boardID)) {
              const lists = data[boardID];
              resolve(lists);
            } else {
              reject("Lists not found for this board ID!!");
            }
          }
        });
      }, 2000);
    })
  }
}

module.exports = getAllListsForBoardID;
