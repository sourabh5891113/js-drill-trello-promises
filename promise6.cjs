const getBoardFromID = require("./promise1.cjs");
const getAllListsForBoardID = require("./promise2.cjs");
const getAllCardsForListID = require("./promise3.cjs");

function getThanosInfoAndAllListsCards(boardID) {
  if (typeof boardID === "string") {
    setTimeout(() => {
      return getBoardFromID(boardID)
        .then((board) => {
          console.log("Board Info:", board);
          return getAllListsForBoardID(board.id);
        })
        .then((lists) => {
          console.log("Lists for Thanos Board:");
          lists.forEach((list,index)=>{
            getAllCardsForListID(list.id).then(cards => {
              console.log("==> "+ list.name + " list");
              console.log(`Cards in ${list.name} list:`, cards);
            })
            .catch(() => {
              console.log("==> "+ list.name + " list");
              console.log(`Cards not available for ${list.name} list`);
            });
          })
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }, Math.random() * 3000);
  } else {
    console.log("All arguments should be a string");
  }
}

module.exports = getThanosInfoAndAllListsCards;
