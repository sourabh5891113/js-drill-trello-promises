/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in
     boards.json and use promises.
*/
const getBoardFromID = require("../promise1.cjs");

let boardID = "mcu453ed";

getBoardFromID(boardID)
  .then((boardInfo) => {
    console.log(boardInfo.name + "'s info is ", boardInfo);
  })
  .catch((error) => {
    console.error(error);
  });
