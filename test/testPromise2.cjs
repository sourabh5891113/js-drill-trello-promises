/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data 
    in lists.json and use promises.
*/
const getAllListsForBoardID = require("../promise2.cjs");

let boardID = "mcu453ed";
getAllListsForBoardID(boardID)
  .then((lists) => {
    console.log("Lists for boardID = " + boardID);
    console.log(lists);
  })
  .catch((error) => {
    console.error(error);
  });
