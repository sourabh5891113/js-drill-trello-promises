const getBoardFromID = require("./promise1.cjs");
const getAllListsForBoardID = require("./promise2.cjs");
const getAllCardsForListID = require("./promise3.cjs");

function getThanosInfoAndMindSpace(boardID, listName1, listName2) {
  if (
    typeof boardID === "string" &&
    typeof listName1 === "string" &&
    typeof listName2 === "string"
  ) {
    setTimeout(() => {
      return getBoardFromID(boardID)
        .then((board) => {
          console.log("Board Info:", board);
          return getAllListsForBoardID(board.id);
        })
        .then((lists) => {
          console.log("Lists for Thanos Board:", lists);
          const mindList = lists.find((list) => list.name === listName1);
          const spaceList = lists.find((list) => list.name === listName2);
          if (mindList && spaceList) {
            return Promise.all([
              getAllCardsForListID(mindList.id),
              getAllCardsForListID(spaceList.id),
            ]);
          } else {
            throw new Error(
              (listName1 ? listName1 : listName2) +
                " list not found in Thanos board"
            );
          }
        })
        .then(([mindcards, spacecards]) => {
          console.log("Cards in " + listName1 + " list:", mindcards);
          console.log("Cards in " + listName2 + " list:", spacecards);
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }, Math.random() * 3000);
  } else {
    console.log("All arguments should be a string");
  }
}

module.exports = getThanosInfoAndMindSpace;
