const fs = require("fs");

function getAllCardsForListID(listID) {
  if (typeof listID === "string") {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        fs.readFile("../data/cards.json", "utf-8", (err, data) => {
          if (err) {
            reject(err);
          } else {
            data = JSON.parse(data);
            if (data.hasOwnProperty(listID)) {
              const cards = data[listID];
              resolve(cards);
            } else {
              reject("Cards not found for this list ID!!");
            }
          }
        });
      }, Math.random() * 2000);
    });
  } else {
    console.log("listID should be a string");
  }
}

module.exports = getAllCardsForListID;
