const fs = require("fs");

function getBoardFromID(boardID) {
  if (typeof boardID === "string") {
    return new Promise((resolve,reject)=>{
      fs.readFile("../data/boards.json", "utf-8", (err, data) => {
        if (err) {
          reject(err);
        } else {
          data = JSON.parse(data);
          let borad = data.find((board) => board.id === boardID);
          if(borad !== undefined){
              setTimeout(resolve,1000,borad);
          }else{
              reject("Board not found!!");
          }
        }
      });
    })
  }
}

module.exports = getBoardFromID;
